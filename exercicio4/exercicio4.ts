/* Faça um programa que receba o salário de um funcionário,
calcule e mostre o novo salário, sabendo-se que este sofreu
um aumento de 25%.*/

namespace exercicio4 
{
    let salario1, novo_salario: number;

    salario1 = 1000;

    novo_salario = salario1 + ( salario1 * 25/100 );

    console.log(`O novo salário será de: ${novo_salario}`)
    
}
// Faça um programa que receba quatros números inteiros, calcule e mostre a soma desses números.
// Primeiro bloco ( Início ) 
namespace exercicio1
{
    // Entrada dos dados 
    //var - let - const
    let numero1, numero2, numero3, numero4: number;

    numero1 = 6; 
    numero2 = 7;
    numero3 = 9;
    numero4 = 11;

    let resultado: number;

    // processar os dados 
    resultado = numero1 + numero2 + numero3 + numero4;

    // saída 
    console.log("O resultado da soma é: " + resultado + "\n");
    // ou pode se escrever 
    console.log(`O resultado da soma é: ${resultado}`);


}
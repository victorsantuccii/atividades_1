/* Faça um programa que receba o salário de um funcionário e
o percentual de aumento, calcule e mostre o valor do
aumento e o novo salário. */

namespace exercicio5
{
    let salario1, aumento: number;

    salario1 = 1000;
    aumento = 20;

    let salario_total: number;

    aumento = salario1 * aumento /100;

    salario_total = salario1 + aumento;

    console.log(`O aumento será de ${aumento}`);
    console.log(`O novo salário será: ${salario_total}`);


}
/* Faça um programa que receba três notas, calcule e mostre a
média ponderada entre elas. */

namespace exercicio3 
{
    let nota1, nota2, nota3, peso1, peso2, peso3 : number;

    nota1 = 10;
    nota2 = 8;
    nota3 = 9;
    peso1 = 2;
    peso2 = 3;
    peso3 = 4;
 
    let media_p: number;

    media_p = ( nota1 * peso1 + nota2 * peso2 * + nota3 * peso3 ) / ( peso1 + peso2 + peso3 );

    console.log(` O resultado da média será de ${media_p}`);

}
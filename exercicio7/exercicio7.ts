/* Faça um programa que receba o salário-base de um

funcionário, calcule e mostre o seu salário a receber, sabendo-
se que esse funcionário tem gratificação de R$50,00 e paga

imposto de 10% sobre o salário-base. */

namespace exercicio7
{
    let salario_base, salario_final, gratificacao, imposto;

    salario_base = 1000;
    gratificacao = 50;
    imposto = 10;

    imposto = salario_base * imposto/100;

    salario_final = salario_base + gratificacao - imposto;

    console.log(` O salário final será de: ${salario_final}`);
    console.log(` O imposto será de: ${imposto}`);

}
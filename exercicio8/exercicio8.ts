/* Faça um programa que receba o valor de um depósito e o
valor da taxa de juros, calcule e mostre o valor do rendimento
e o valor total depois do rendimento. */

namespace exercicio8
{
    let valor_total, taxa_juros, deposito, rendimento;

    deposito = 100;
    taxa_juros = 10;

    rendimento = deposito * taxa_juros/100;
    valor_total = deposito + rendimento;

    console.log(` O valor do rendimento ao final será de ${rendimento}`);
    console.log(` O valor total será de: ${valor_total}`);
    

}